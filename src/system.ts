import type { Model } from "./types"

export const defaultSetting = {
  continuousDialogue: true,
  archiveSession: false,
  openaiAPIKey: "",
  openaiAPITemperature: 60,
  password: "",
  systemRule: "",
  model: "gpt-3.5-turbo" as Model
}
export const defaultMessage = `
- 网站更新了许多新功能（搜索历史消息,实时花费消耗,多个聊天等）具体可以去 [公众号](gzh.jpg) 文章了解。[NewChat](https://chat2.zhulei.xyz)入口（拥有自己key的体验）
- 防止失联(这个网站进不来，又没有加WX的可以收藏一下备用网站 [备用chat4](https://chat4.zhulei.xyz))、咨询买号(有GPT-4)、买key、接个人镜像站建站可公众号👉 [私信](gzh.jpg)
- 感谢大家的打赏，如果想体验GPT-4的可以关注下👉 [公众号](gzh.jpg) 加一下体验群，后续会更新
- [[Shift]] + [[Enter]] 换行。开头输入 [[/]] 或者 [[空格]] 搜索 Prompt 预设。[[↑]] 可编辑最近一次提问。点击顶部名称滚动到顶部，点击输入框滚动到底部。`

// export const defaultMessage = `
//   - 服了，有人拿脚本刷，钱控制不住了，我暂时不更新免费的key了，网站不会关闭，大家可以用自己的key。我也会在公众号里更新一些key
// - 防止失联(这个网站进不来，又没有加WX的可以收藏一下备用网站 [chat2](https://chat2.zhuleixx.top/))、咨询买号、买key、接个人镜像站建站可 ▷▷▷ [私信](wx.jpg)
// - 感谢大家的打赏，最近反应有点慢，重点:我升级了key有不一样的体验。希望支持我一下😭
// - 最近钱花的有点快，如果网站提示额度不够请及时提醒我  ↑
// - 本站支持最新gpt4体验，(暂时不卖4了，可以输入自己支持4的key体验)可以▷▷▷ [私信](wx.jpg)有更新可第一时间了解
// - 你可以用TA聊天、翻译、创作、编程、撰写文案、营销策划、法律咨询、知识学习等等，提交学习工作效率！如果本项目对你有所帮助，可以给小猫 [买点零食](/zsm.jpg)。
// - 欢迎老板打赏~ 赞赏会用于购买 apikey 维持本站的运转，apikey 成本巨增😭，大家可以用自己的 key 使用本站，自己的 key 成本很低。
// - [[Shift]] + [[Enter]] 换行。开头输入 [[/]] 或者 [[空格]] 搜索 Prompt 预设。[[↑]] 可编辑最近一次提问。点击顶部名称滚动到顶部，点击输入框滚动到底部。`


// export const defaultMessage = `
// - 防止失联、咨询买号、买key,定期更新一些免费的key可 ▷▷▷ [私信](wx.jpg)
// - 感谢大家的打赏，如果大家有用到很慢（很长时间没有给回答）请联系我
// - 最近钱花的有点快，如果网站提示额度不够请及时提醒我  ↑
// - 本站支持最新gpt4体验，可以▷▷▷ [私信](wx.jpg)有更新可第一时间了解
// - 你可以用TA聊天、翻译、创作、编程、撰写文案、营销策划、法律咨询、知识学习等等，提交学习工作效率！如果本项目对你有所帮助，可以给小猫 [买点零食](/zsm.jpg)。
// - 欢迎老板打赏~ 赞赏会用于购买 apikey 维持本站的运转，apikey 成本巨增😭，大家可以用自己的 key 使用本站，自己的 key 成本很低。
// - [[Shift]] + [[Enter]] 换行。开头输入 [[/]] 或者 [[空格]] 搜索 Prompt 预设。[[↑]] 可编辑最近一次提问。点击顶部名称滚动到顶部，点击输入框滚动到底部。`

// export const defaultMessage = `
//   - 感谢 [Diu](https://github.com/ddiu8081) | [ourongxing](https://github.com/ourongxing)

// - 大家好，我是宙斯AI智能聊天机器人，基于最新语言模型的AI人工智能聊天机器人，为你带来AI聊天、翻译、创作、编程、撰写文案、营销策划、法律咨询、知识学习等等，提交学习工作效率！
// - [[Shift]] + [[Enter]] 换行。开头输入 [[/]] 或者 [[空格]] 搜索 Prompt 预设。[[↑]] 可编辑最近一次提问。点击顶部名称滚动到顶部，点击输入框滚动到底部
// `

export type Setting = typeof defaultSetting

export const defaultResetContinuousDialogue = false

export const defaultMaxInputTokens: Record<Model, number> = {
  "gpt-3.5-turbo": 2000,
  "gpt-4": 6144,
  "gpt-4-32k": 24576
}

export const defaultModel: Model = "gpt-3.5-turbo"
